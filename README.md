# AN108 Test

Inspired from https://opentechlab.org.uk/videos:014:notes#design_files. 

## Tools Required 

Open source toolchain (ECP5)

## Pinouts

| AN108 | ULX3S | Notes |
|   --- |   --- | ---   |
|     1 |    5V |       |
|     2 |   GND |       |
|     5 |   14+ | DACLK |
|     6 |   14- | DA7   |
|     7 |   15+ | DA6   |
|     8 |   15- | DA5   |
|     9 |   16+ | DA4   |
|    10 |   16- | DA3   |
|    11 |   17+ | DA2   |
|    12 |   17- | DA1   |
|    13 |   18+ | DA0   |
|    21 |   21+ | AD0   |
|    22 |   21- | AD1   |
|    23 |   22+ | AD2   |
|    24 |   22- | AD3   |
|    25 |   23+ | AD4   |
|    26 |   23- | AD5   |
|    27 |   24+ | AD6   |
|    28 |   24- | AD7   |
|    29 |   25+ | ADCLK |

The max clock speed for ADCLK is 32MHz. For DACLK it is 100MHz.
