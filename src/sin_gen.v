/**
 * SIN generaion module.
 */

module sin_gen #(
                 parameter PW = 8,
                           OW = 12
                )(
               input               clk,
               input               sample_en,
               input [phase_W-1:0] freq,
               output [6:0]        sin_val
               );

    parameter base_clk = 100_000_000; //100MHz
    parameter phase_W = 32;
    localparam NN = (2**phase_W)/base_clk;

    reg [phase_W:0]                phase;
    reg [phase_W:0]                frequency_step, nnh;
    reg [6:0]                      sin_table [0:255];

    initial
        begin
            $readmemh("sin.hex", sin_table);
        end

    always @ (posedge clk)
        begin
            if (sample_en)
                begin
                    phase          <= phase + frequency_step;
                    frequency_step <= 32'd70_000;
                end
        end

    assign sin_val = sin_table[phase[31:24]];

endmodule // sin_gen
