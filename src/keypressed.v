////////////////////////////////////////////////////////////////////////////////
// Filename:    keypressed.v
// Author:      Tom Martin, Jason Thweatt
// Date:        10/24/2013
// Version:     3
// Description: This FSM generates an enable pulse that lasts for one clock
//              period each time the enable_in signal is pressed and released.

// Updated on 18 March 2014 by J.S. Thweatt
// Commented to describe state machine structure

// Updated on 24 March 2015 by J.S. Thweatt
// Output machine separated from register input logic.

//update by Steven Frederiksen on 11/15/2016 

module keypressed(clock, reset, enable_in, enable_out);
	input  clock;			// The system clock. (Connect to an FPGA clock signal in the top-level module.)
	input	 reset;			// Active-low reset. (Connect to a pushbutton in the top-level module.)
	input  enable_in;		// Enable input signal. (Connect to a pushbutton in the top-level module.)
	output enable_out;	// The output is high for one FPGA clock cycle each time enable_in is pressed and released.
	reg    enable_out;
	
// Variables for keeping track of the state.
	reg [1:0] key_state, next_key_state;

// Set up parameters for the "state" of the pushbutton.
// Since there are three states, we are using 2 bits to represent the state in a so-called "dense" assignment.
	parameter KEY_FREE = 2'b00, KEY_PRESSED = 2'b01, KEY_RELEASED = 2'b10;

	always @(posedge clock) begin
	
		if(reset == 1'b1)
			key_state <= KEY_FREE;
		
		else
			key_state <= next_key_state;

	end

	always@(key_state or enable_in) begin

		next_key_state = key_state;
		
// Use the present state to determine the next state.

		case(key_state)
		
			KEY_FREE: begin
				if(enable_in == 1'b0)
					next_key_state = KEY_PRESSED;
			end
			KEY_PRESSED: begin
				if(enable_in == 1'b1)
					next_key_state = KEY_RELEASED;
			end
			

			KEY_RELEASED: next_key_state = KEY_FREE;
			
			default: next_key_state = KEY_FREE;
			
		endcase
	end

	always@(key_state) begin
	
// Use the present state to determine the output.

		case(key_state)
		
// If the key is currently unpressed and was not just released, enable_out should be 0.

			KEY_FREE:     enable_out = 1'b0;

// If the key is currently being pressed, enable_out should be 0.

			KEY_PRESSED:  enable_out = 1'b0;

// If the key has has just gone from being pressed to being released, enable out should be 1.
// Since this state only lasts for one clock cycle, enable_out is 1 for only one clock cycle.

			KEY_RELEASED: enable_out = 1'b1;

// If none of the above - something that should never happen - make the output unknown.
// In synthesis, this will allow the behavior of "invalid" input combinations to be optimized away.

			default:      enable_out = 1'b1;
			
		endcase
	end
	
endmodule
