

module an108 (
              input          clk_25mhz,
              //input [7:0]  ad,
              input [2:0]    btn,
              output reg [7:0]   led,
              output reg [7:0]  da,
              input [7:0] ad,
              output       ad_clk,
              output       da_clk,
              output       wifi_gpio0
              );

    // get out of the way
    assign wifi_gpio0 = 1;

    /*AUTOWIRE*/
    // Beginning of automatic wires (for undeclared instantiated-module outputs)
    wire [6:0]          sin_val;                // From sin of sin_gen.v
    // End of automatics
    wire                clkout0, clkout1, clk;
    /* verilator lint_off UNUSED */
    wire                locked;
    /* verilator lint_on UNUSED */
    wire                reset;
    //localparam ctr_width          = 32;
    localparam phase_W      = 32;
    //reg [ctr_width-1:0] ctr = 0;
    //reg [7:0]           o_led, o_da;
    wire                sample_en;
    wire [phase_W-1:0]  freq     = 1630; // Hz
    reg [6:0]           counter_val, next_counter_val;
    wire                increment;
    wire                sig_i;
`ifdef VERILATOR
    assign clkout0 = clk_25mhz;
    assign clkout1 = clk_25mhz;
    assign clk = clkout0;
    assign da_clk = clkout1;
    assign locked = 1'b1;
`else
    /**
     * PLL
     * clk0 100MHz
     * clk1 30MHz
     */
    pll_100
        clocks(
               .clkin(clk_25mhz),
               // Outputs
               .clkout0                 (clk),
               .clkout1                 (clkout1),
               .locked                  (locked));
`endif

    assign da_clk = clkout1;
    assign ad_clk = clkout1;
    always @(posedge clkout1) begin
        led <= ad;
        da <= ad;
    end


endmodule // an108
