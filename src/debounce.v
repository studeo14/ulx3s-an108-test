

module debounce #(
                  parameter WAIT_CYCLES = 4000
                  )(
                  input clk,
                  input reset_n,
                  input sig_i,
                  output sig_o
                    );
    localparam WAIT_FOR_UP = 1, SIGNAL = 2, WAIT_FOR_DOWN = 3;

    reg [11:0] input_buffer, next_input_buffer;
    reg                   sig_o_r, next_sig_o_r;
    reg [2:0]             state, next_state;

    always @ (posedge clk)
        begin
            if (!reset_n)
                begin
                    input_buffer <= 0;
                    sig_o_r      <= 0;
                    state        <= WAIT_FOR_UP;
                end
            else
                begin
                    input_buffer <= next_input_buffer;
                    sig_o_r      <= next_sig_o_r;
                    state        <= next_state;
                end
        end

    always @ (*)
        begin
            next_sig_o_r      = 0;
            next_state        = state;
            next_input_buffer = input_buffer;
            case(state)
                WAIT_FOR_UP:
                    begin
                        if (input_buffer >= WAIT_CYCLES)
                            begin
                                next_state        = SIGNAL;
                                next_input_buffer = 0;
                            end
                        else if (sig_i)
                            begin
                                next_input_buffer = input_buffer + 1;
                            end
                    end
                SIGNAL:
                    begin
                        next_sig_o_r = 1;
                        next_state   = WAIT_FOR_DOWN;
                    end
                WAIT_FOR_DOWN:
                    begin
                        if (input_buffer >= WAIT_CYCLES)
                            begin
                                next_state        = WAIT_FOR_UP;
                                next_input_buffer = 0;
                            end
                        else if (sig_i)
                            begin
                                next_input_buffer = input_buffer + 1;
                            end
                    end
            endcase
        end

    assign sig_o = sig_o_r;

endmodule // debounce
