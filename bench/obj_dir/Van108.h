// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _VAN108_H_
#define _VAN108_H_  // guard

#include "verilated_heavy.h"

//==========

class Van108__Syms;

//----------

VL_MODULE(Van108) {
  public:
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(clk_25mhz,0,0);
    VL_IN8(btn,3,0);
    VL_OUT8(led,7,0);
    VL_OUT8(da,7,0);
    VL_OUT8(da_clk,0,0);
    VL_OUT8(wifi_gpio0,0,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    CData/*7:0*/ an108__DOT__counter_val;
    CData/*0:0*/ an108__DOT__btn_1_debounce__DOT__sig_o_r;
    CData/*6:0*/ an108__DOT__sin__DOT__sin_val_o;
    WData/*4095:0*/ an108__DOT__btn_1_debounce__DOT__input_buffer[128];
    WData/*4095:0*/ an108__DOT__btn_1_debounce__DOT__next_input_buffer[128];
    QData/*32:0*/ an108__DOT__sin__DOT__phase;
    QData/*32:0*/ an108__DOT__sin__DOT__frequency_step;
    QData/*32:0*/ an108__DOT__sin__DOT__nnh;
    CData/*6:0*/ an108__DOT__sin__DOT__sin_table[256];
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    CData/*0:0*/ __Vclklast__TOP__clk_25mhz;
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    Van108__Syms* __VlSymsp;  // Symbol table
    
    // CONSTRUCTORS
  private:
    VL_UNCOPYABLE(Van108);  ///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible with respect to DPI scope names.
    Van108(const char* name = "TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~Van108();
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(Van108__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(Van108__Syms* symsp, bool first);
  private:
    static QData _change_request(Van108__Syms* __restrict vlSymsp);
  public:
    static void _combo__TOP__2(Van108__Syms* __restrict vlSymsp);
    static void _combo__TOP__5(Van108__Syms* __restrict vlSymsp);
  private:
    void _ctor_var_reset() VL_ATTR_COLD;
  public:
    static void _eval(Van108__Syms* __restrict vlSymsp);
  private:
#ifdef VL_DEBUG
    void _eval_debug_assertions();
#endif  // VL_DEBUG
  public:
    static void _eval_initial(Van108__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    static void _eval_settle(Van108__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    static void _initial__TOP__1(Van108__Syms* __restrict vlSymsp) VL_ATTR_COLD;
    static void _sequent__TOP__4(Van108__Syms* __restrict vlSymsp);
    static void _settle__TOP__3(Van108__Syms* __restrict vlSymsp) VL_ATTR_COLD;
} VL_ATTR_ALIGNED(VL_CACHE_LINE_BYTES);

//----------


#endif  // guard
