// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Van108.h for the primary calling header

#include "Van108.h"
#include "Van108__Syms.h"

//==========

VL_CTOR_IMP(Van108) {
    Van108__Syms* __restrict vlSymsp = __VlSymsp = new Van108__Syms(this, name());
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    _ctor_var_reset();
}

void Van108::__Vconfigure(Van108__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Van108::~Van108() {
    delete __VlSymsp; __VlSymsp=NULL;
}

void Van108::eval() {
    VL_DEBUG_IF(VL_DBG_MSGF("+++++TOP Evaluate Van108::eval\n"); );
    Van108__Syms* __restrict vlSymsp = this->__VlSymsp;  // Setup global symbol table
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
#ifdef VL_DEBUG
    // Debug assertions
    _eval_debug_assertions();
#endif  // VL_DEBUG
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        VL_DEBUG_IF(VL_DBG_MSGF("+ Clock loop\n"););
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("../src/../src/an108.v", 3, "",
                "Verilated model didn't converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

void Van108::_eval_initial_loop(Van108__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    // Evaluate till stable
    int __VclockLoop = 0;
    QData __Vchange = 1;
    do {
        _eval_settle(vlSymsp);
        _eval(vlSymsp);
        if (VL_UNLIKELY(++__VclockLoop > 100)) {
            // About to fail, so enable debug to see what's not settling.
            // Note you must run make with OPT=-DVL_DEBUG for debug prints.
            int __Vsaved_debug = Verilated::debug();
            Verilated::debug(1);
            __Vchange = _change_request(vlSymsp);
            Verilated::debug(__Vsaved_debug);
            VL_FATAL_MT("../src/../src/an108.v", 3, "",
                "Verilated model didn't DC converge\n"
                "- See DIDNOTCONVERGE in the Verilator manual");
        } else {
            __Vchange = _change_request(vlSymsp);
        }
    } while (VL_UNLIKELY(__Vchange));
}

void Van108::_initial__TOP__1(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_initial__TOP__1\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->wifi_gpio0 = 1U;
    VL_READMEM_N(true, 7, 256, 0, std::string("sin.hex")
                 , vlTOPp->an108__DOT__sin__DOT__sin_table
                 , 0, ~VL_ULL(0));
}

VL_INLINE_OPT void Van108::_combo__TOP__2(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_combo__TOP__2\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->da_clk = vlTOPp->clk_25mhz;
}

void Van108::_settle__TOP__3(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_settle__TOP__3\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->da_clk = vlTOPp->clk_25mhz;
    vlTOPp->da = vlTOPp->an108__DOT__counter_val;
    vlTOPp->led = ((8U & (IData)(vlTOPp->btn)) ? (IData)(vlTOPp->an108__DOT__sin__DOT__sin_val_o)
                    : (IData)(vlTOPp->an108__DOT__counter_val));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[1U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[1U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[2U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[1U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[2U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[3U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[2U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[3U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[4U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[3U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[4U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[5U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[4U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[5U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[6U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[5U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[6U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[7U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[6U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[7U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[8U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[7U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[8U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[9U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[8U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[9U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xaU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[9U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xaU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xbU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xaU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xbU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xcU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xbU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xcU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xdU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xcU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xdU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xeU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xdU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xeU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xfU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xeU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xfU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x10U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xfU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x10U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x11U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x10U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x11U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x12U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x11U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x12U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x13U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x12U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x13U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x14U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x13U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x14U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x15U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x14U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x15U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x16U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x15U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x16U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x17U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x16U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x17U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x18U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x17U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x18U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x19U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x18U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x19U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x19U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x20U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x20U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x21U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x20U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x21U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x22U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x21U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x22U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x23U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x22U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x23U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x24U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x23U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x24U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x25U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x24U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x25U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x26U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x25U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x26U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x27U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x26U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x27U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x28U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x27U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x28U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x29U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x28U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x29U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x29U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x30U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x30U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x31U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x30U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x31U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x32U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x31U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x32U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x33U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x32U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x33U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x34U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x33U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x34U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x35U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x34U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x35U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x36U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x35U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x36U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x37U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x36U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x37U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x38U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x37U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x38U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x39U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x38U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x39U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x39U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x40U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x40U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x41U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x40U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x41U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x42U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x41U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x42U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x43U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x42U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x43U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x44U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x43U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x44U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x45U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x44U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x45U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x46U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x45U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x46U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x47U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x46U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x47U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x48U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x47U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x48U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x49U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x48U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x49U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x49U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x50U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x50U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x51U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x50U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x51U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x52U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x51U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x52U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x53U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x52U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x53U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x54U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x53U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x54U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x55U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x54U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x55U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x56U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x55U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x56U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x57U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x56U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x57U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x58U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x57U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x58U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x59U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x58U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x59U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x59U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x60U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x60U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x61U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x60U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x61U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x62U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x61U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x62U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x63U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x62U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x63U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x64U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x63U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x64U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x65U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x64U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x65U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x66U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x65U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x66U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x67U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x66U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x67U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x68U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x67U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x68U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x69U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x68U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x69U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x69U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x70U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x70U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x71U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x70U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x71U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x72U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x71U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x72U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x73U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x72U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x73U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x74U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x73U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x74U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x75U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x74U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x75U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x76U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x75U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x76U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x77U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x76U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x77U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x78U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x77U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x78U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x79U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x78U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x79U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x79U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7fU] 
        = ((0x80000000U & ((IData)(vlTOPp->btn) << 0x1eU)) 
           | (0x7fffffffU & (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7fU] 
                             >> 1U)));
}

VL_INLINE_OPT void Van108::_sequent__TOP__4(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_sequent__TOP__4\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    QData/*32:0*/ __Vdly__an108__DOT__sin__DOT__phase;
    // Body
    __Vdly__an108__DOT__sin__DOT__phase = vlTOPp->an108__DOT__sin__DOT__phase;
    if ((4U & (IData)(vlTOPp->btn))) {
        __Vdly__an108__DOT__sin__DOT__phase = (VL_ULL(0x1ffffffff) 
                                               & (vlTOPp->an108__DOT__sin__DOT__phase 
                                                  + vlTOPp->an108__DOT__sin__DOT__frequency_step));
        vlTOPp->an108__DOT__sin__DOT__sin_val_o = vlTOPp->an108__DOT__sin__DOT__sin_table
            [(0xffU & (IData)((vlTOPp->an108__DOT__sin__DOT__phase 
                               >> 0x18U)))];
    }
    if ((1U & (IData)(vlTOPp->btn))) {
        vlTOPp->an108__DOT__counter_val = ((IData)(vlTOPp->an108__DOT__btn_1_debounce__DOT__sig_o_r)
                                            ? (0xffU 
                                               & ((IData)(1U) 
                                                  + (IData)(vlTOPp->an108__DOT__counter_val)))
                                            : 0U);
    }
    vlTOPp->an108__DOT__sin__DOT__phase = __Vdly__an108__DOT__sin__DOT__phase;
    if ((4U & (IData)(vlTOPp->btn))) {
        vlTOPp->an108__DOT__sin__DOT__frequency_step 
            = (VL_ULL(0x1ffffffff) & VL_DIV_QQQ(33, vlTOPp->an108__DOT__sin__DOT__nnh, VL_ULL(0x17d7840)));
    }
    vlTOPp->da = vlTOPp->an108__DOT__counter_val;
    vlTOPp->an108__DOT__btn_1_debounce__DOT__sig_o_r 
        = ((IData)(vlTOPp->btn) & (0U == ((((((((((
                                                   ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0U]) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[1U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[2U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[3U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[4U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[5U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[6U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[7U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[8U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[9U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xaU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xbU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xcU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xdU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xeU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xfU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x10U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x11U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x12U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x13U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x14U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x15U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x16U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x17U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x18U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x19U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1aU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1bU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1cU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1dU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1eU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1fU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x20U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x21U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x22U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x23U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x24U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x25U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x26U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x27U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x28U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x29U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2aU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2bU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2cU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2dU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2eU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2fU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x30U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x31U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x32U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x33U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x34U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x35U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x36U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x37U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x38U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x39U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3aU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3bU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3cU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3dU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3eU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3fU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x40U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x41U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x42U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x43U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x44U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x45U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x46U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x47U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x48U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x49U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4aU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4bU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4cU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4dU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4eU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4fU])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x50U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x51U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x52U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x53U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x54U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x55U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x56U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x57U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x58U])) 
                                                                                | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x59U])) 
                                                                               | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5aU])) 
                                                                              | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5bU])) 
                                                                             | (~ 
                                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5cU])) 
                                                                            | (~ 
                                                                               vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5dU])) 
                                                                           | (~ 
                                                                              vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5eU])) 
                                                                          | (~ 
                                                                             vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5fU])) 
                                                                         | (~ 
                                                                            vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x60U])) 
                                                                        | (~ 
                                                                           vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x61U])) 
                                                                       | (~ 
                                                                          vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x62U])) 
                                                                      | (~ 
                                                                         vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x63U])) 
                                                                     | (~ 
                                                                        vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x64U])) 
                                                                    | (~ 
                                                                       vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x65U])) 
                                                                   | (~ 
                                                                      vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x66U])) 
                                                                  | (~ 
                                                                     vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x67U])) 
                                                                 | (~ 
                                                                    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x68U])) 
                                                                | (~ 
                                                                   vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x69U])) 
                                                               | (~ 
                                                                  vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6aU])) 
                                                              | (~ 
                                                                 vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6bU])) 
                                                             | (~ 
                                                                vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6cU])) 
                                                            | (~ 
                                                               vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6dU])) 
                                                           | (~ 
                                                              vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6eU])) 
                                                          | (~ 
                                                             vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6fU])) 
                                                         | (~ 
                                                            vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x70U])) 
                                                        | (~ 
                                                           vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x71U])) 
                                                       | (~ 
                                                          vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x72U])) 
                                                      | (~ 
                                                         vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x73U])) 
                                                     | (~ 
                                                        vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x74U])) 
                                                    | (~ 
                                                       vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x75U])) 
                                                   | (~ 
                                                      vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x76U])) 
                                                  | (~ 
                                                     vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x77U])) 
                                                 | (~ 
                                                    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x78U])) 
                                                | (~ 
                                                   vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x79U])) 
                                               | (~ 
                                                  vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7aU])) 
                                              | (~ 
                                                 vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7bU])) 
                                             | (~ vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7cU])) 
                                            | (~ vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7dU])) 
                                           | (~ vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7eU])) 
                                          | (~ vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7fU]))));
    if ((4U & (IData)(vlTOPp->btn))) {
        vlTOPp->an108__DOT__sin__DOT__nnh = VL_ULL(0);
    }
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[1U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[1U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[2U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[2U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[3U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[3U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[4U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[4U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[5U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[5U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[6U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[6U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[7U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[7U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[8U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[8U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[9U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[9U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xaU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xaU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xbU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xbU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xcU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xcU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xdU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xdU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xeU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xeU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xfU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xfU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x10U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x10U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x11U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x11U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x12U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x12U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x13U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x13U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x14U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x14U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x15U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x15U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x16U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x16U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x17U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x17U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x18U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x18U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x19U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x19U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1aU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1aU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1bU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1bU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1cU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1cU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1dU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1dU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1eU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1eU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1fU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1fU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x20U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x20U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x21U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x21U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x22U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x22U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x23U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x23U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x24U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x24U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x25U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x25U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x26U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x26U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x27U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x27U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x28U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x28U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x29U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x29U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2aU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2aU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2bU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2bU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2cU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2cU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2dU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2dU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2eU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2eU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2fU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2fU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x30U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x30U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x31U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x31U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x32U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x32U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x33U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x33U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x34U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x34U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x35U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x35U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x36U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x36U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x37U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x37U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x38U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x38U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x39U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x39U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3aU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3aU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3bU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3bU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3cU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3cU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3dU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3dU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3eU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3eU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3fU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3fU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x40U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x40U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x41U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x41U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x42U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x42U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x43U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x43U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x44U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x44U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x45U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x45U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x46U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x46U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x47U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x47U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x48U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x48U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x49U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x49U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4aU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4aU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4bU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4bU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4cU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4cU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4dU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4dU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4eU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4eU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4fU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4fU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x50U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x50U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x51U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x51U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x52U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x52U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x53U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x53U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x54U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x54U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x55U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x55U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x56U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x56U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x57U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x57U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x58U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x58U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x59U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x59U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5aU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5aU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5bU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5bU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5cU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5cU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5dU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5dU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5eU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5eU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5fU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5fU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x60U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x60U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x61U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x61U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x62U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x62U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x63U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x63U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x64U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x64U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x65U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x65U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x66U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x66U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x67U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x67U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x68U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x68U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x69U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x69U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6aU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6aU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6bU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6bU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6cU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6cU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6dU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6dU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6eU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6eU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6fU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6fU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x70U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x70U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x71U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x71U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x72U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x72U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x73U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x73U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x74U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x74U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x75U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x75U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x76U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x76U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x77U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x77U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x78U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x78U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x79U] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x79U]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7aU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7aU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7bU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7bU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7cU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7cU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7dU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7dU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7eU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7eU]
            : 0U);
    vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7fU] 
        = ((1U & (IData)(vlTOPp->btn)) ? vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7fU]
            : 0U);
}

VL_INLINE_OPT void Van108::_combo__TOP__5(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_combo__TOP__5\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->led = ((8U & (IData)(vlTOPp->btn)) ? (IData)(vlTOPp->an108__DOT__sin__DOT__sin_val_o)
                    : (IData)(vlTOPp->an108__DOT__counter_val));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[1U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[1U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[2U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[1U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[2U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[3U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[2U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[3U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[4U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[3U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[4U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[5U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[4U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[5U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[6U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[5U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[6U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[7U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[6U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[7U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[8U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[7U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[8U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[9U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[8U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[9U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xaU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[9U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xaU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xbU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xaU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xbU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xcU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xbU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xcU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xdU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xcU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xdU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xeU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xdU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xeU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xfU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xeU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0xfU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x10U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0xfU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x10U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x11U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x10U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x11U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x12U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x11U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x12U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x13U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x12U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x13U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x14U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x13U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x14U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x15U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x14U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x15U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x16U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x15U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x16U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x17U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x16U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x17U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x18U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x17U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x18U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x19U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x18U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x19U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x19U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x1fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x20U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x1fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x20U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x21U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x20U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x21U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x22U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x21U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x22U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x23U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x22U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x23U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x24U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x23U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x24U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x25U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x24U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x25U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x26U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x25U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x26U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x27U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x26U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x27U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x28U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x27U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x28U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x29U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x28U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x29U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x29U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x2fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x30U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x2fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x30U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x31U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x30U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x31U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x32U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x31U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x32U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x33U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x32U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x33U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x34U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x33U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x34U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x35U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x34U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x35U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x36U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x35U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x36U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x37U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x36U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x37U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x38U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x37U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x38U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x39U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x38U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x39U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x39U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x3fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x40U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x3fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x40U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x41U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x40U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x41U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x42U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x41U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x42U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x43U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x42U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x43U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x44U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x43U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x44U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x45U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x44U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x45U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x46U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x45U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x46U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x47U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x46U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x47U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x48U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x47U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x48U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x49U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x48U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x49U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x49U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x4fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x50U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x4fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x50U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x51U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x50U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x51U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x52U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x51U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x52U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x53U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x52U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x53U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x54U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x53U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x54U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x55U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x54U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x55U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x56U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x55U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x56U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x57U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x56U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x57U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x58U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x57U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x58U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x59U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x58U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x59U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x59U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x5fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x60U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x5fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x60U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x61U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x60U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x61U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x62U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x61U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x62U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x63U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x62U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x63U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x64U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x63U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x64U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x65U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x64U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x65U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x66U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x65U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x66U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x67U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x66U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x67U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x68U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x67U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x68U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x69U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x68U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x69U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x69U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x6fU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x70U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x6fU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x70U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x71U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x70U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x71U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x72U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x71U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x72U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x73U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x72U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x73U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x74U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x73U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x74U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x75U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x74U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x75U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x76U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x75U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x76U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x77U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x76U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x77U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x78U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x77U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x78U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x79U] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x78U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x79U] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7aU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x79U] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7aU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7bU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7aU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7bU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7cU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7bU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7cU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7dU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7cU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7dU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7eU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7dU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7eU] 
        = ((vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7fU] 
            << 0x1fU) | (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7eU] 
                         >> 1U));
    vlTOPp->an108__DOT__btn_1_debounce__DOT__next_input_buffer[0x7fU] 
        = ((0x80000000U & ((IData)(vlTOPp->btn) << 0x1eU)) 
           | (0x7fffffffU & (vlTOPp->an108__DOT__btn_1_debounce__DOT__input_buffer[0x7fU] 
                             >> 1U)));
}

void Van108::_eval(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_eval\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_combo__TOP__2(vlSymsp);
    if (((IData)(vlTOPp->clk_25mhz) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk_25mhz)))) {
        vlTOPp->_sequent__TOP__4(vlSymsp);
    }
    vlTOPp->_combo__TOP__5(vlSymsp);
    // Final
    vlTOPp->__Vclklast__TOP__clk_25mhz = vlTOPp->clk_25mhz;
}

void Van108::_eval_initial(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_eval_initial\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP__1(vlSymsp);
    vlTOPp->__Vclklast__TOP__clk_25mhz = vlTOPp->clk_25mhz;
}

void Van108::final() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::final\n"); );
    // Variables
    Van108__Syms* __restrict vlSymsp = this->__VlSymsp;
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Van108::_eval_settle(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_eval_settle\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__3(vlSymsp);
}

VL_INLINE_OPT QData Van108::_change_request(Van108__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_change_request\n"); );
    Van108* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}

#ifdef VL_DEBUG
void Van108::_eval_debug_assertions() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_eval_debug_assertions\n"); );
    // Body
    if (VL_UNLIKELY((clk_25mhz & 0xfeU))) {
        Verilated::overWidthError("clk_25mhz");}
    if (VL_UNLIKELY((btn & 0xf0U))) {
        Verilated::overWidthError("btn");}
}
#endif  // VL_DEBUG

void Van108::_ctor_var_reset() {
    VL_DEBUG_IF(VL_DBG_MSGF("+    Van108::_ctor_var_reset\n"); );
    // Body
    clk_25mhz = VL_RAND_RESET_I(1);
    btn = VL_RAND_RESET_I(4);
    led = VL_RAND_RESET_I(8);
    da = VL_RAND_RESET_I(8);
    da_clk = VL_RAND_RESET_I(1);
    wifi_gpio0 = VL_RAND_RESET_I(1);
    an108__DOT__counter_val = VL_RAND_RESET_I(8);
    VL_RAND_RESET_W(4096, an108__DOT__btn_1_debounce__DOT__input_buffer);
    VL_RAND_RESET_W(4096, an108__DOT__btn_1_debounce__DOT__next_input_buffer);
    an108__DOT__btn_1_debounce__DOT__sig_o_r = VL_RAND_RESET_I(1);
    an108__DOT__sin__DOT__phase = VL_RAND_RESET_Q(33);
    an108__DOT__sin__DOT__sin_val_o = VL_RAND_RESET_I(7);
    an108__DOT__sin__DOT__frequency_step = VL_RAND_RESET_Q(33);
    an108__DOT__sin__DOT__nnh = VL_RAND_RESET_Q(33);
    { int __Vi0=0; for (; __Vi0<256; ++__Vi0) {
            an108__DOT__sin__DOT__sin_table[__Vi0] = VL_RAND_RESET_I(7);
    }}
}
